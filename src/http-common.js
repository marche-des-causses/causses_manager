const axios = require("axios");

const baseURL = process.env.VUE_APP_API_ADDRESS + "/";

const instance = axios.create({
  baseURL: baseURL,
  timeout: 10000,
  withCredentials: false,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS, PATCH",
    "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
  },
  validateStatus: function (status) {
    return status >= 200 && status < 300;
  }
});

module.exports = {
  get: (endpoint, params, token, timeout) => {
    return instance.request({
      url: endpoint,
      method: "get",
      params: params,
      headers: {
        Authorization: token
      },
      timeout: timeout
    });
  },

  delete: (endpoint, params, body, token, timeout) => {
    return instance.request({
      url: endpoint,
      method: "delete",
      params: params,
      data: body,
      headers: {
        Authorization: token
      },
      timeout: timeout
    });
  },

  post: (endpoint, params, body, token, timeout) => {
    return instance.request({
      url: endpoint,
      method: "post",
      params: params,
      data: body,
      headers: {
        Authorization: token
      },
      timeout: timeout
    });
  },

  put: (endpoint, params, body, token, timeout) => {

    return instance.request({
      url: endpoint,
      method: "put",
      params: params,
      data: body,
      headers: {
        Authorization: token
      },
      timeout: timeout
    });
  },

  patch: (endpoint, params, body, token, timeout) => {
    return instance.request({
      url: endpoint,
      method: "patch",
      params: params,
      data: body,
      headers: {
        Authorization: token
      },
      timeout: timeout
    });
  }

};
