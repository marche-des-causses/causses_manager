import Vue from "vue";
import VueRouter from "vue-router";
import views from "../views/index.js";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: views.Home
  },
  {
    path: "/me",
    name: "Profile",
    component: views.Profile,
    children: [
      {
        path: "infos",
        name: "ProfilInfos",
        alias: "",
        component: views.ProfileInfos
      },
      {
        path: "details",
        name: "ProfilDetails",
        component: views.ProfileDetails
      }
    ]
  },
  {
    path: "/api",
    name: "API",
    component: views.API,
    children: [
      {
        path: "home",
        name: "APIHome",
        alias: "",
        component: views.APIHome
        // redirect: { name: 'YearList' }
      },
      {
        path: "years",
        name: "YearList",
        alias: "/years",
        components: {
          default: views.Years
        }
      },
      {
        path: "teams",
        name: "TeamList",
        alias: "/teams",
        components: {
          default: views.Teams
        }
      },
      {
        path: "caussenards",
        name: "CaussenardList",
        alias: "/caussenards",
        components: {
          default: views.Caussenards
        },
        beforeEnter (to, from, next) {
          if (!localStorage.authLevel || localStorage.authLevel < 2) {
            next({ name: "APIHome" });
          } else {
            next();
          }
        }
      },
      {
        path: "users",
        name: "UserList",
        alias: "/users",
        components: {
          default: views.Users
        },
        beforeEnter (to, from, next) {
          if (!localStorage.authLevel || localStorage.authLevel < 5) {
            next({ name: "APIHome" });
          } else {
            next();
          }
        }
      },
      {
        path: "year/:id",
        name: "Year",
        alias: "/year/:id",
        components: {
          default: views.Year
        },
        props: {
          default: true
        }
      },
      {
        path: "team/:id",
        name: "Team",
        alias: "/team/:id",
        components: {
          default: views.Team
        },
        props: {
          default: true
        }
      },
      {
        path: "caussenard/:id",
        name: "Caussenard",
        alias: "/caussenard/:id",
        components: {
          default: views.Caussenard
        },
        props: {
          default: true
        },
        beforeEnter (to, from, next) {
          if (!localStorage.authLevel || localStorage.authLevel < 2) {
            next({ name: "APIHome" });
          } else {
            next();
          }
        }
      },
      {
        path: "user/:id",
        name: "User",
        alias: "/user/:id",
        components: {
          default: views.User
        },
        props: {
          default: true
        },
        beforeEnter (to, from, next) {
          if (!localStorage.authLevel || localStorage.authLevel < 5) {
            next({ name: "APIHome" });
          } else {
            next();
          }
        }
      }
    ]
  },
  {
    path: "/stats",
    name: "Stats",
    component: views.Stats,
    children: [
      {
        path: "home",
        name: "StatsHome",
        alias: "",
        component: views.StatsHome
      },
      {
        path: "years",
        name: "YearStats",
        component: views.YearStats,
        props: (route) => ({ id: route.query.id })
      },
      {
        path: "teams",
        name: "TeamStats",
        component: views.TeamStats,
        props: (route) => ({ id: route.query.id })
      }
    ]
  },
  {
    path: "/team-maker",
    name: "TeamMakers",
    component: views.TeamMakers,
    children: [
      {
        path: "new",
        name: "TeamMakerNew",
        component: views.TeamMakerCreator
      },
      {
        path: "historic",
        name: "TeamMakerHistoric",
        component: views.TeamMakerHistoric
      },
      {
        path: ":id",
        name: "TeamMakerId",
        components: {
          default: views.TeamMaker
        },
        props: {
          default: true
        }
      }
    ]
  },
  {
    path: "/about",
    name: "About",
    component: views.About
  },
  {
    path: "/advanced",
    name: "Advanced",
    alias: "/advanced",
    components: {
      default: views.Advanced
    },
    beforeEnter (to, from, next) {
      if (!localStorage.authLevel || localStorage.authLevel < 2) {
        next({ name: "Home" });
      } else {
        next();
      }
    }
  },
  {
    path: "/contact",
    name: "Contact",
    component: views.Contact
  },
  {
    path: "/api-docs",
    name: "APIDoc",
    component: views.APIDocumentation
  },
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: views.PageNotFound
    // redirect: { name : "Home"}
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return { selector: to.hash };
    } else if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  }
});

export default router;
