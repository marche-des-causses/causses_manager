


module.exports = {

  average(data) {
    var sum = 0;
    data.forEach((element) => {
      sum += element.stats;
    });
    return Math.round(sum/data.length * 10) / 10;
  },

  standardDeviation(data, average) {
    var sum2 = 0;
    data.forEach((element) => {
      sum2 += Math.pow(element.stats - average, 2);
    });
    return Math.round(Math.sqrt(sum2/data.length) * 10) / 10;
  }

};