import Home from "@/views/Home.vue";
import About from "@/views/About.vue";
import Contact from "@/views/Contact.vue";
import APIDocumentation from "@/views/APIDocumentation.vue";
import Advanced from "@/views/Advanced.vue";
import PageNotFound from "@/views/errors/PageNotFound.vue";
import Profile from "@/views/Profile.vue";
import ProfileInfos from "@/views/Profile/ProfileInfos.vue";
import ProfileDetails from "@/views/Profile/ProfileDetails.vue";

import API from "@/views/API.vue";
import APIHome from "@/views/API/APIHome.vue";
import Years from "@/views/API/Years.vue";
import Teams from "@/views/API/Teams.vue";
import Caussenards from "@/views/API/Caussenards.vue";
import Users from "@/views/API/Users.vue";
import Year from "@/views/API/Year.vue";
import Team from "@/views/API/Team.vue";
import Caussenard from "@/views/API/Caussenard.vue";
import User from "@/views/API/User.vue";

import Stats from "@/views/Stats.vue";
import StatsHome from "@/views/Stats/StatsHome.vue";
import YearStats from "@/views/Stats/Year.vue";
import TeamStats from "@/views/Stats/Team.vue";
import CaussenardStats from "@/views/Stats/Caussenard.vue";

import TeamMakers from "@/views/TeamMakers.vue";
import TeamMaker from "@/views/TeamMaker/TeamMaker.vue";
import TeamMakerCreator from "@/views/TeamMaker/TeamMakerCreator.vue";
import TeamMakerHistoric from "@/views/TeamMaker/TeamMakerHistoric.vue";

export default {
  Home,
  About,
  Contact,
  APIDocumentation,
  Advanced,
  PageNotFound,
  Profile,
  ProfileDetails,
  ProfileInfos,
  API,
  APIHome,
  Year,
  Years,
  Team,
  Teams,
  Caussenard,
  Caussenards,
  User,
  Users,
  Stats,
  StatsHome,
  YearStats,
  TeamStats,
  CaussenardStats,
  TeamMakers,
  TeamMaker,
  TeamMakerCreator,
  TeamMakerHistoric
};
