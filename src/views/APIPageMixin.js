import { mapState } from "vuex";

export default {
  data () {
    return {
      refreshCounter: 0,
      pageName: "",
      searchText: ""
    };
  },
  mounted: function () {
    this.$store.commit("api/newActivePage", this.pageName);
  },
  methods: {
    refresh: function () {
      this.refreshCounter += 1;
    },
  },
  computed: {
    ...mapState("account", {
      token: "token"
    })
  },
  watch: {
    token: function(newVal) {
      if (newVal != null) {
        if (localStorage.email) {
          this.refresh();
        } else {
          this.$router.go();
        }
      }
    }
  }
};
