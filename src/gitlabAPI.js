const axios = require("axios");

const instance = axios.create({
  baseURL: "https://gitlab.com/api/v4/",
  timeout: 10000,
  withCredentials: false,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS, PATCH",
    "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
  },
  validateStatus: function (status) {
    return status >= 200 && status < 300;
  }
});

module.exports = {
  get: (endpoint, params, token) => {
    return instance.request({
      url: endpoint,
      method: "get",
      params: params,
      headers: {
        Authorization: token
      }
    });
  },

  delete: (endpoint, params, body, token) => {
    return instance.request({
      url: endpoint,
      method: "delete",
      params: params,
      data: body,
      headers: {
        Authorization: token
      }
    });
  },

  post: (endpoint, params, body, token) => {
    return instance.request({
      url: endpoint,
      method: "post",
      params: params,
      data: body,
      headers: {
        Authorization: token
      }
    });
  },

  put: (endpoint, params, body, token) => {
    return instance.request({
      url: endpoint,
      method: "put",
      params: params,
      data: body,
      headers: {
        Authorization: token
      }
    });
  },

  patch: (endpoint, params, body, token) => {
    return instance.request({
      url: endpoint,
      method: "patch",
      params: params,
      data: body,
      headers: {
        Authorization: token
      }
    });
  }

};
