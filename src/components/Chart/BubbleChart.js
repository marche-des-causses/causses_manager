import { Bubble, mixins } from "vue-chartjs";
const { reactiveProp } = mixins;


export default {
  extends: Bubble,
  mixins: [reactiveProp],
  props: {
    chartData: {
      type: Object,
      default: null
    },
    options: {
      type: Object,
      default: null
    }
  },
  mounted () {
    if (this.chartData != null) {
      this.renderChart(this.chartData, this.options);
    }
  },
};
