import { Line, mixins } from "vue-chartjs";
const { reactiveProp } = mixins;


export default {
  extends: Line,
  mixins: [reactiveProp],
  props: {
    chartData: {
      default: null
    },
    options: {
      default: null
    }
  },
  mounted () {
    if (this.chartData != null) {
      this.renderChart(this.chartData, this.options);
    }
  },
};
