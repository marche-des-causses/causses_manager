


export default {
  props: {
    displayTitle: {
      default: false
    },
    startDate: {
      default: 1976
    },
    endDate: {
      default: 2100
    }
  },
};
