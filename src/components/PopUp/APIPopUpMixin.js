import { mapState, mapActions } from "vuex";

export default {
  props: ["active"],
  computed: {
    ...mapState("api", {
      notif: "notif"
    })
  },
  watch: {
    active: function (newValue) {
      if (newValue == true) {
        this.closeNotif();
      }
    }
  },
  methods: {
    ...mapActions("api", {
      closeNotif: "closeNotif"
    }),

    closePopUp () {
      this.closeNotif();
      this.$emit("close");
    }
  }
};
