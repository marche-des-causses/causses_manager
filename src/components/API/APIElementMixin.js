
export default {
  props: {
    refreshCounter: {
      type: Number
    }
  },
  methods: {
    refresh () {
      this.$emit("refresh");
    }
  },
  watch: {
    refreshCounter: function () {
      this.onRefresh();
    }
  }
};
