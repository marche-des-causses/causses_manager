
export default {
  methods: {
    incrementIndex () {
      this.setSortIndex((this.sortIndex === this.nbOfSorts - 1) ? 0 : this.sortIndex + 1);
    },

    changeOrder () {
      this.setSortOrder((this.sortOrder === "ASC") ? "DESC" : "ASC");
    },

    changeIcon () {
      this.$refs.sortIcon.innerHTML = "<i class=\"fas fa-sort-" +
          ((this.sortObjectType === "String") ? "alpha" : "numeric") +
          "-" + ((this.sortOrder === "ASC") ? "up" : "down") + "-alt\">";
    }
  },
  mounted: function () {
    this.changeIcon();
  },
  watch: {
    sortObjectType: function () {
      this.changeIcon();
    },
    sortOrder: function () {
      this.changeIcon();
    }
  },
  computed: {
    sortObjectType: function () {
      return this.sortObject.type;
    }
  }
};
