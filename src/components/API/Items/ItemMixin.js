import { mapGetters } from "vuex";
import HTTP from "@/http-common";

export default {
  data () {
    return {
      active: false
    };
  },
  props: {
    elementContent: Object,
    endPoint: String
  },
  methods: {
    deleteElement () {
      HTTP.delete(this.endPoint + this.elementContent.id,
        null, null, this.$store.state.account.token
      ).then(() => {
        this.$emit("refresh");
      }).catch((error) => {
        this.$store.dispatch("api/displayError", error);
      });
    }
  },
  computed: {
    ...mapGetters("account", {
      isAdmin: "isAdmin",
      isEditor: "isEditor"
    })
  }
};
