
import { mapGetters } from "vuex";
import HTTP from "@/http-common";

export default {

  props: ["endPoint"],
  data () {
    return {
      elementContent: {
        type: Object,
        default: {}
      },
      loading: false,
      askDelete: false,
      askUpdate: false,
      deleteRedirect: "APIHome"
    };
  },
  methods: {
    getElement () {
      HTTP.get(this.endPoint, null, this.$store.state.account.token)
        .then((response) => {
          this.elementContent = response.data.content;
          this.loading = false;
        }).catch((error) => {
          this.$store.dispatch("api/displayError", error);
        });
    },
    deleteElement () {
      HTTP.delete(this.endPoint, null, null, this.$store.state.account.token)
        .then(() => {
          this.$router.replace({ name: this.deleteRedirect });
        }).catch((error) => {
          this.$store.dispatch("api/displayError", error);
        });
    },

    onRefresh () {
      //this.loading = true;
      this.getElement();
    }
  },
  mounted: function () {
    this.onRefresh();
  },
  computed: {
    // Authentication
    ...mapGetters("account", {
      isEditor: "isEditor",
      isManager: "isManager"
    })
  }
};
