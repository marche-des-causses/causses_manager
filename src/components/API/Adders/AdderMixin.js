
export default {
  data () {
    return {
      notifMessage: "Notification",
      notifDetails: "",
      notifType: "danger",
      notifDisplay: false
    };
  },
  methods: {
    clearForm () {},
    success () {
      this.notifMessage = "Ajout réussi";
      this.notifDetails = "";
      this.notifType = "success";
      this.notifDisplay = true;
      this.clearForm();
      this.refresh();
    },
    error (error) {
      this.notifMessage = error.response.data.error.userMessage;
      this.notifDetails = error.response.data.error.devMessage;
      this.notifType = "danger";
      this.notifDisplay = true;
    },
    closeNotif () {
      this.notifDisplay = false;
    },
    onRefresh () {}
  }
};
