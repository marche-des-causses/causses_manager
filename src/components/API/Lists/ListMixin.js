import { mapMutations, mapState } from "vuex";
import HTTP from "@/http-common";

export default {
  data () {
    return {
      listOfElements: [],
      loading: false,
    };
  },
  props: {
    endPoint: String,
    pagination: Boolean,
    refreshCounter: Number,
    searchText: String
  },
  methods: {
    ...mapMutations("api/caussenardsList", {
      setNbOfElement: "setNbOfElement",
      setSortIndex: "newSortIndex",
      setSortOder: "newSortOrder"
    }),

    ...mapMutations("api", {
      setElemByPage: "newElemByPage"
    }),

    getElementslist () {
      HTTP.get(this.path, (this.pagination) ? {
        limit: this.elemByPage,
        offset: this.offset,
        order: this.order,
        search: this.searchText
      } : {
        order: this.order,
        search: this.searchText
      }, this.$store.state.account.token
      ).then((response) => {
        this.listOfElements = response.data.content;
        this.$store.dispatch("api/closeNotif");
        this.loading = false;
      }).catch((error) => {
        this.$store.dispatch("api/displayError", error);
      });
    },

    getNbOfElements () {
      HTTP.get(this.path, { count: true, search: this.searchText },
        this.$store.state.account.token
      ).then((response) => {
        this.setNbOfElement(response.data.content);
        this.getElementslist();
      }).catch((error) => {
        this.$store.dispatch("api/displayError", error);
      });
    },

    onRefresh: function () {
      this.loading = true;
      if (this.pagination) {
        this.getNbOfElements();
      } else {
        this.getElementslist();
      }
    }
  },
  mounted () {
    this.onRefresh();
  },
  watch: {
    elemByPage: function (newValue, oldValue) {
      if (newValue !== oldValue) {
        this.setPage(this.currentPage);
        this.getElementslist();
      }
    },
    currentPage: function (newValue, oldValue) {
      if (newValue !== oldValue) {
        this.getElementslist();
      }
    },
    order: function (newValue, oldValue) {
      if (newValue !== oldValue) {
        this.getElementslist();
      }
    }
  },
  computed: {
    // Content
    ...mapState("api", {
      elemByPage: "elemByPage"
    }),

    path: function() {
      return (!this.searchText || this.searchText.length == 0) ? this.endPoint : this.endPoint + "search/";
    }
  }
};
