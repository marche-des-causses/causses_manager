
export default {
  props: {
    value: {
      type: [String, Number, Boolean]
    },
    details: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      isValid: false,
      isEmpty: true,
      content: this.value
    };
  },
  methods: {
    check: function () {},
    handleInput (e) {
      this.$emit("input", e.target.value);
    },
    keyup (e) {
      if (this.isValid && e.keyCode == 13) 
        this.$emit("validate");
    }
  },
  watch: {
    value: function (newValue) {
      this.content = newValue;
      this.check({ target: { value: newValue } });
    }
  },
  computed: {
    message: function () {
      return (this.isEmpty) ? "" : ((this.isValid) ? this.messageOk : this.messageNOK);
    },
    theClass: function () {
      return (this.isEmpty) ? "" : ((this.isValid) ? "is-success" : "is-danger");
    },
    icon: function () {
      return (this.isEmpty) ? "" : ((this.isValid) ? "fas fa-check" : "fas fa-exclamation-triangle");
    }
  }
};
