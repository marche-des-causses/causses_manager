
export default {
  watch: {
    content: function (newValue, lastValue) {
      if (newValue !== lastValue) {
        this.$emit("input", newValue);
      }
    }
  }
};
