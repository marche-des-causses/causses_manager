
export default {
  data () {
    return {
      elementList: Array
    };
  },
  mounted () {
    this.getList();
  },
  methods: {
    getList () {},
    
    handleChange (e) {
      this.$emit("input", e.target.value);
    }
  },
  computed: {
    noElements: function () {
      return (this.elementList.length === 0);
    }
  }
};
