import Vue from "vue";
import Vuex from "vuex";
import actions from "./actions";
import mutations from "./mutations";
import account from "./modules/account";
import api from "./modules/api";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    logged: false
  },
  mutations: mutations,
  actions: actions,
  modules: {
    account,
    api
  },
  strict: process.env.NODE_ENV !== "production"
});
