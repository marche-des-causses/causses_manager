
const MyReusableModule = {
  namespaced: false,
  state: () => ({
    active: false
  }),

  getters: {},

  actions: {
    open ({ commit }, condition) {
      if (condition) {
        commit("activePopUp");
      }
    },

    close ({ commit }) {
      commit("resetForm");
      commit("closePopUp");
      commit("closeNotif");
    }
  },

  mutations: {
    activePopUp (state) {
      state.active = true;
    },

    closePopUp (state) {
      state.active = false;
    }
  }
};

export default MyReusableModule;
