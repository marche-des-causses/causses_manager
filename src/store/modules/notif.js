
const MyReusableModule = {
  namespaced: false,
  state: () => ({
    message: "",
    details: "",
    type: "",
    active: false
  }),

  getters: {},

  actions: {
    closeNotif ({ commit }) {
      commit("closeNotif");
    },
    displayNotif ({ commit }, notif) {
      commit("displayNotif", notif);
    }
  },

  mutations: {
    displayNotif (state, notif) {
      state.active = true;
      state.message = notif.message;
      state.details = notif.details;
      state.type = notif.type;
    },

    closeNotif (state) {
      state.active = false;
      state.details = "";
      state.message = "";
      state.type = "";
    }
  }
};

export default MyReusableModule;
