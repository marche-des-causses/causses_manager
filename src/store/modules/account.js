import HTTP from "@/http-common";

// initial state
const state = {
  connected: false,
  authLevel: 0,
  waitingLogging: false,
  id: null,
  email: null,
  caussenard: {
    firstname: "",
    lastname: ""
  },
  token: null
};

// getters
const getters = {
  isLogged: (state) => {
    return (state.email != null);
  },

  isAdmin: (state) => {
    if (state.authLevel == 5) {
      return true;
    } else {
      return false;
    }
  },

  isManager: (state) => {
    if (state.authLevel >= 4) {
      return true;
    } else {
      return false;
    }
  },

  isEditor: (state) => {
    if (state.authLevel >= 3) {
      return true;
    } else {
      return false;
    }
  },

  isVerified: (state) => {
    if (state.authLevel >= 2) {
      return true;
    } else {
      return false;
    }
  }
};

// actions
const actions = {

  getAPIKey ({ dispatch }) {
    HTTP.get(
      "api/token", null, null, null
    ).then((response) => {
      dispatch("setToken", response.data.content.token);
    });
  },

  disconnect ({ commit, dispatch }) {
    localStorage.removeItem("token");
    localStorage.removeItem("email");
    localStorage.removeItem("id");
    localStorage.removeItem("authLevel");
    commit("deleteAccount");
    dispatch("getAPIKey");
  },

  saveAccount ({ commit }, user) {
    localStorage.email = user.email;
    localStorage.id = user.id;
    localStorage.authLevel = user.authLevel;
    commit("saveAccount", user);
  },

  setToken ({ commit }, token) {
    localStorage.token = token;
    commit("setToken", token);
  }

};

// mutations
const mutations = {

  saveAccount (state, user) {
    state.waitingLogging = false;
    state.email = user.email;
    state.id = user.id;
    state.authLevel = user.authLevel;
  },

  deleteAccount (state) {
    state.token = null;
    state.email = null;
    state.id = null;
    state.authLevel = 0;
  },

  setToken (state, token) {
    state.token = token;
  }

};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
