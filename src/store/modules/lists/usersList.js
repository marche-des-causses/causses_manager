
import list from "./list";

// initial state
const state = {
  sortBy: [
    {
      name: "email",
      type: "String",
      key: "email"
    }
  ]
};

// getters
const getters = {

  sortObject: (state, getters) => {
    return state.sortBy[getters.sortIndex];
  },

  order: (state, getters) => {
    return getters.sortObject.key + ":" + getters.sortOrder;
  },

  nbOfSorts: (state) => {
    return state.sortBy.length;
  }
};

// actions
const actions = {
  setSortIndex ({ commit, getters }, index) {
    if (index < getters.nbOfSorts) {
      commit("newSortIndex", index);
    }
  },
  setSortOrder ({ commit }, order) {
    if (order === "ASC" || order === "DESC") {
      commit("newSortOrder", order);
    }
  }
};

// mutations
const mutations = {
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    list: list
  }
};
