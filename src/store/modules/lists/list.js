import notif from "../notif";

const MyReusableModule = {
  namespaced: false,
  state: () => ({
    page: 1,
    nbOfElement: 1,
    sort: {
      index: 0,
      order: "ASC"
    }
  }),

  getters: {
    maxPage: (state, getters, rootState) => {
      return Math.ceil(state.nbOfElement / rootState.api.elemByPage);
    },

    offset: (state, getters, rootState) => {
      return (state.page - 1) * rootState.api.elemByPage;
    },

    sortOrder: (state) => {
      return state.sort.order;
    },

    sortIndex: (state) => {
      return state.sort.index;
    },

    currentPage: (state) => {
      return state.page;
    }
  },

  actions: {
    setPage ({ getters, commit }, page) {
      commit("newPage", Math.max(1, Math.min(page, getters.maxPage)));
    }
  },

  mutations: {
    newPage (state, page) {
      state.page = page;
    },
    newSortIndex (state, index) {
      state.sort.index = index;
    },
    newSortOrder (state, order) {
      state.sort.order = order;
    },
    setNbOfElement (state, nb) {
      state.nbOfElement = nb;
    }
  },

  modules: {
    notif: notif
  }
};

export default MyReusableModule;
