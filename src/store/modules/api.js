
import notif from "./notif";
import yearsList from "./lists/yearsList";
import teamsList from "./lists/teamsList";
import caussenardsList from "./lists/caussenardsList";
import usersList from "./lists/usersList";

// initial state
const state = {
  elemByPage: 20,
  activePage: "home"
};

// getters
const getters = {
};

// actions
const actions = {

  displayError ({ commit, dispatch }, error) {
    if (error.response) {
      if (error.response.data.error) {
        if (error.response.data.error.code === 201) {
          dispatch("account/disconnect", null, { root: true });
          return 2;
        } else if (error.response.data.error.code === 204) {
          dispatch("account/getAPIKey", null, { root: true });
        } else {
          commit("displayNotif", {
            type: "danger",
            message: error.response.data.error.userMessage,
            details: error.response.data.error.devMessage.toString()
          });
        }
      } else {
        commit("displayNotif", {
          type: "danger",
          message: "Error",
          details: error.response.data.toString()
        });
      }
    } else if (error.request) {
      commit("displayNotif", {
        type: "danger",
        message: "Error: " + error.message
      });
      return 1;
    } else {
      commit("displayNotif", {
        type: "danger",
        message: "Error",
        details: error.message
      });
    }
    return 0;
  },

  displaySuccess ({ commit }, message) {
    commit("displayNotif", {
      type: "success",
      message: message
    });
  }
};

// mutations
const mutations = {
  newElemByPage (state, elements) {
    if (elements === 10 || elements === 20 || elements === 50 || elements === 100) {
      state.elemByPage = elements;
    } else {
      elements = 20;
    }
  },

  newActivePage (state, page) {
    state.activePage = page;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    notif,
    yearsList,
    teamsList,
    caussenardsList,
    usersList
  }
};
